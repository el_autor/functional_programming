(defun get_list (lst)
    (car
        (mapcan #'(lambda (el)
            (cond
                (
                    (and
                        (listp el)
                        (not (null el))
                    )
                    (cons el nil)
                )
            )
        ) lst
        )
    )
)