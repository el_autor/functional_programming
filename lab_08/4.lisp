(defun minus_all (lst)
    (mapcar #'(lambda (el)
        (cond
            (
                (numberp el)
                (- el 10)
            )
            (   
                (atom el)
                el
            )
            (
                (minus_all el)
            )
        )
    ) lst
    )
)