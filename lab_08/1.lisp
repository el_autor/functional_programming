(defun multiply_num (lst n)
    (mapcar #'(lambda (elem)
        (* elem n)
    ) lst
    )
)

(defun multiply_all (lst n)
    (mapcar #'(lambda (el)
        (cond
            (
                (numberp el)
                (* el n)
            )
            (
                (atom el)
                el
            )
            (
                (multiply_all el n)
            )   
        )
    ) lst   
    )    
)