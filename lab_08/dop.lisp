(defun sum_all (lst)
    (reduce '+
        (mapcar #'(lambda (el)
            (cond
                (
                    (numberp el)
                    el
                )
                (
                    (atom el)
                    0
                )
                (
                    (sum_all el)
                )
            )
        ) lst
        )
    )
)