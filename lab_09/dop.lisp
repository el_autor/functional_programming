(defun squares (lst)
    (mapcar #'(lambda (el)
        (cond
            (
                (numberp el)
                (* el el)
            )
            (   
                (atom el)
                el
            )
            (
                (squares el)
            )
        )
    ) lst
    )
)