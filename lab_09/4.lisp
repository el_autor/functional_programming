(defun count_length (list_of_lists)
    (reduce #'+
        (mapcar #'(lambda (x)
            (cond
                (
                    (atom x)
                    1
                )
                (
                    (count_length x)
                )
            )
        ) list_of_lists
        )
    )
)