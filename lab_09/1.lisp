(defun select_between (lst a b)
    (mapcan #'(lambda (x)
        (cond
            (
                (and
                    (numberp x)
                    (or 
                        (and (>= x a) (<= x b))
                        (and (<= x a) (>= x b))
                    ) 
                )
                (cons x nil)
            )
            (
                (listp x)
                (select_between x a b)
            )
        )
    ) lst
    )
)