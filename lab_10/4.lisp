(defun get_last (lst)
    (cond
        (
            (null (cdr lst))
            (car lst)
        )
        (   
            (get_last (cdr lst))
        )
    )
)