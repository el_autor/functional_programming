(defun alloddr_wrapper (lst flag)
    (cond
        (
            (or (null lst) (not flag))
            flag
        )
        (
            (listp (car lst))
            (alloddr_wrapper 
                (cdr lst)
                (alloddr_wrapper (car lst) flag)
            )
        )
        (
            (and
                (numberp (car lst))
                (oddp (car lst))
            )
            (alloddr_wrapper (cdr lst) flag)
        )
    )
)

(defun alloddr (lst)
    (alloddr_wrapper lst t)
)