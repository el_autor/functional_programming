(defun rec_nth (lst index)
    (cond
        (
            (null lst)
            nil
        )
        (
            (= index 0)
            (car lst)
        )
        (
            (rec_nth (cdr lst) (- index 1))
        )
    )
)