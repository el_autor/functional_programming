(defun rec_add_wrapper (lst accum)
    (cond
        (
            (null lst)
            accum
        )
        (
            (listp (car lst))
            (rec_add_wrapper (cdr lst) (rec_add_wrapper (car lst) accum))
        )
        (
            (numberp (car lst))
            (rec_add_wrapper (cdr lst) (+ (car lst) accum))
        )
        (
            (rec_add_wrapper (cdr lst) accum)
        )
    )
)

(defun rec_add (lst)
    (rec_add_wrapper lst 0)
)