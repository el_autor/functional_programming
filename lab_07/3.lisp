(defun find_in_table (table argument)
    (reduce #'(lambda (x y) (or x y))
        (mapcar #'(lambda (pair)
            (cond
                (
                    (equal (car pair) argument)
                    (cdr pair)
                )
                (
                    (equal (cdr pair) argument)
                    (car pair)
                )
            )
        ) table
    )
    )
)