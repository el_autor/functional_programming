(defun is_palindrom (lst)
    (cond
        (
            (null lst)
        )
        (
            (reduce 
                #'(lambda (x y) (and x y))
                (mapcar #'equal lst (reverse lst))
            )
        )
    )
)