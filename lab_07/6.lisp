(defun swap_to_left (lst k)
    (cond
        (
            (<= k 0)
            lst
        )
        (
            (swap_to_left
                (nconc
                    (cdr lst)
                    (cons (car lst) nil)
                )
                (- k 1)
            )
        )
    )
)

(defun swap_to_right (lst k)
    (cond
        (
            (<= k 0)
            lst
        )
        (
            (swap_to_right
                (nconc
                    (mapcon #'(lambda (el)
                        (cond
                            (
                                (null (cdr el))
                                el
                            )
                        )
                    ) lst
                    )
                    (mapcon #'(lambda (el)
                        (cond
                            (
                                (cdr el)
                                (cons (car el) nil)
                            )
                        )
                    ) lst
                    )
                )
                (- k 1)
            )
        )
    )
)