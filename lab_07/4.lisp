(defun swap_first_last (lst)
    (cond
        (
            (null (cdr lst))
            lst
        )
        (
            (nconc
                (mapcon #'(lambda (elem)
                    (cond
                        (
                            (null (cdr elem))
                            elem
                        )
                    )
                ) lst
                )
                (mapcon #'(lambda (elem)
                    (cond
                         (
                            (cdr elem)
                            (cons (car elem) nil)
                         )
                    )
                ) (cdr lst)
                )
                (cons (car lst) nil)
            )
        )
    )
)