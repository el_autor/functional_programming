(defun swap_two_elements (lst index1 index2)
    (cond
        (
            (null (nth index1 lst))
            lst
        )
        (
            (null (nth index2 lst))
            lst
        )
        (
            (= index1 index2)
            lst
        )
        (
            (nconc
                (subseq
                    lst
                    0 
                    (if (< index1 index2)
                        index1
                        index2
                    )
                )
                (cons
                    (if (< index1 index2)
                        (nth index2 lst)
                        (nth index1 lst)
                    )
                    nil
                )
                (subseq lst
                    (if (< index1 index2)
                        (+ index1 1)
                        (+ index2 1)
                    )

                    (if (< index1 index2)
                        index2
                        index1
                    )
                )
                (cons 
                    (if (< index1 index2)
                        (nth index1 lst)
                        (nth index2 lst)
                    )
                    nil
                )
                (subseq lst 
                    (if (< index1 index2)
                        (+ index2 1)
                        (+ index1 1)
                    )
                )
            )
        )
    )
)